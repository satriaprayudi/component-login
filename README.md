# bit-vue-tutorial

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

# Bit

## Add component

```
bit add src/components/{component_name}/{component_name}.vue --id {component_name}
bit add src/components/{component_name}/{component_name}.js --id {component_name}
...
```

## Build component

```
bit build
```

## Tag component

```
bit tag --all 0.0.1
```

## Export component

```
bit export <username>.<scope>
```

## Import component to other project

cmd

```
$ bit import satryacode.dashboard/component-login
```

router/index.js

```
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Login',
    name: 'Login',
    component: () => import('@bit/satryacode.dashboard.component-login/Login.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  if (to.path === "/"){
    try {
      let authUser = JSON.parse(sessionStorage.getItem('authUser'));
      let token = authUser["token"];
      if(token){
        next(); console.log("to Home");
      }
    } catch (error) {
      console.log("Please login first")
      window.location.replace("/Login");
    }
  }
  next(); //this is needed
});

export default router

```

# About Component

## Shared Login Info

```
let data = sessionStorage.getItem('authUser');
```
